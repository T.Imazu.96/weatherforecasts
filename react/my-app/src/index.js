import React from 'react';
import ReactDOM from 'react-dom';
import {observable, computed, action} from 'mobx';

function TableList(props) {
  return <td>{props.value}</td>;
}

function WeatherForecastsTable(props) {
  const dayForecast = props.dayForecast;
  const tableList = dayForecast.map((row) => 
    <TableList key = {row.id} value = {row.content} />
  );

  return (
    <table id="forecasts_table" border="1">
      <thead>
        <tr>
          <th>今日</th>
          <th>明日</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          {tableList}
        </tr>
      </tbody>
    </table>
  );
}

class WeatherForecast extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false
    };
  }

  componentDidMount() {
    return fetch('http://weather.livedoor.com/forecast/webservice/json/v1?city=140010')
    .then((response) => response.json())
    .then((responseJson) => {
      console.log(responseJson);
      this.setState({
        loading: true,
        json: responseJson,
        title: responseJson.title,
        description: responseJson.description,
        today: responseJson.forecasts[0].telop,
        tomorrow: responseJson.forecasts[1].telop
      })
    });
  }

  render() {
    if(this.state.loading) {
      const dayForecast = [
        {id: 1, content: this.state.today},
        {id: 2, content: this.state.tomorrow}
      ];
      return (
        <div className="weatherForecast">
          <div className="weatherForecast--header">
            <h1 id="title">{this.state.title}</h1>
            <div id="description">{this.state.description.text}</div>
          </div>
          <div className="weatherForecast--table">
            <WeatherForecastsTable dayForecast = {dayForecast}/>
          </div>
        </div>
      );
    } else {
      return (
        <div className="weatherForecast">
          <p>Now Loading...</p>
        </div>
      )
    }
  }
}

ReactDOM.render(
  <WeatherForecast />,
  document.getElementById('root')
);
