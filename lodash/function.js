var weatherJson = 0;

async function getWeatherForecast( cityID ) {
    //fetch('http://weather.livedoor.com/forecast/webservice/json/v1?city=' + cityID)
    //.then(function(response) { return response.json() })
    //.then(function(data) {
    //    parseJson(data);
    //})
    //.catch(function() {
    //    console.log('error.')
    //});
    const res = await fetch('http://weather.livedoor.com/forecast/webservice/json/v1?city=' + cityID);
    const data = await res.json();
    return data;
}

function parseJson(data) {
    weatherJson = data;
    console.log(weatherJson);
    document.getElementById('title').innerHTML = weatherJson['title'];
    document.getElementById('description').innerHTML = weatherJson.description.text;
    var forecasts_table = document.getElementById('forecasts_table');

    _.forEach(weatherJson.forecasts, (value, key) => {
        forecasts_table.rows[1].cells[key].innerHTML = value.telop;
    });
    
    _.forEach(weatherJson.pinpointLocations, (value, key) => {
        var btn = document.createElement('BUTTON');
	btn.textContent = value.name;
	btn.addEventListener('click', () => {
	    location.href = value.link;
	});
	document.getElementById('sample').appendChild(btn);
    });
    
}

window.onload = function() {
    getWeatherForecast( 140010 )
    .then(data => {
        parseJson(data);
    });
}
