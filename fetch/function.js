var weatherJson = 0;

function weatherForecast( cityID ) {
    fetch('http://weather.livedoor.com/forecast/webservice/json/v1?city=' + cityID)
    .then(function(response) { return response.json() })
    .then(function(data) {
        parseJson(data);
    })
    //.catch(function() {
    //    console.log('error.')
    //});
}

function parseJson( data ) {
    console.log(data);
    weatherJson = data;
    document.getElementById('title').innerHTML = data['title'];
	document.getElementById('description').innerHTML = data.description.text;
	var forecasts_table = document.getElementById('forecasts_table');

    for(let k in data.forecasts) {
        forecasts_table.rows[1].cells[k].innerHTML = data.forecasts[k].telop;
    }

    var form = document.forms.mainForm;
    for (let k in data.pinpointLocations) {
        var btn = document.createElement('BUTTON');
        btn.textContent = data.pinpointLocations[k].name;
        btn.addEventListener('click', jump);
        document.getElementById('sample').appendChild(btn);
    }
}

function jump() {
    var obj = event.target;
    for (let k in weatherJson.pinpointLocations)
        if (weatherJson.pinpointLocations[k].name == obj.innerHTML)
            if (confirm(obj.innerHTML + 'のピンポイント天気予報に移動しますか？')==true)
                location.href = weatherJson.pinpointLocations[k].link;
}

window.onload = function() {
    weatherForecast( 140010 ); // 横浜
}

