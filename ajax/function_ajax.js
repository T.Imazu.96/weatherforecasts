var weatherJson = 0;

function getWeatherForecast( cityID ) {
    return new Promise((resolve, reject) => {
        var xhr = new XMLHttpRequest();
        xhr.open('GET', 'http://weather.livedoor.com/forecast/webservice/json/v1?city=' + cityID);
        xhr.responseType = 'json';
        xhr.addEventListener('load', (event) => {
            if(event.target.status !==200) {
                reject();
            }
            resolve(event.target.response);
        });
        xhr.addEventListener('error', () => {
            reject();
        });
        xhr.send();
    });
}

function parseJson(data) {
    weatherJson = data;
    console.log(weatherJson);
    document.getElementById('title').innerHTML = weatherJson['title'];
    document.getElementById('description').innerHTML = weatherJson.description.text;
    var forecasts_table = document.getElementById('forecasts_table');

    for(let k in weatherJson.forecasts) {
        forecasts_table.rows[1].cells[k].innerHTML = weatherJson.forecasts[k].telop;
    }

    for (let k in weatherJson.pinpointLocations) {
        var btn = document.createElement('BUTTON');
        btn.textContent = weatherJson.pinpointLocations[k].name;
        btn.addEventListener('click', jump);
        document.getElementById('sample').appendChild(btn);
    }
}

function jump() {
    var obj = event.target;
    for (let k in weatherJson.pinpointLocations)
        if (weatherJson.pinpointLocations[k].name == obj.innerHTML)
            if (confirm(obj.innerHTML + 'のピンポイント天気予報に移動しますか？')==true)
                location.href = weatherJson.pinpointLocations[k].link;
}

window.onload = function() {
    getWeatherForecast(140010)
    .then((response) => parseJson(response))
    .catch((error) =>  {
        console.error('error');
    });
}
