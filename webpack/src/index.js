var parseString = require('xml2js').parseString;

function getCityJson(xmlText) {
  return new Promise((resolve) => {
    parseString(xmlText, {
      trim: true,
      explicitArray: false},
      function(_, json) {
        resolve(json);
      })
  });
}

async function setCityDropDown() {
  var xmlPromise = await fetch('http://weather.livedoor.com/forecast/rss/primary_area.xml');
  var xmlText = await xmlPromise.text();
  var json = await getCityJson(xmlText);
  var cityJson = json.rss.channel['ldWeather:source'].pref;

  _.forEach(cityJson, (value, key) => {
    var dropright = document.createElement('li');
    dropright.className = 'dropright drop-hover';

    var dropdownItemPrefecture = document.createElement('a');
    dropdownItemPrefecture.className = 'dropdown-item dropdown-toggle';
    dropdownItemPrefecture.href = '#';
    dropdownItemPrefecture.setAttribute('data-toggle', 'dropdown');
    dropdownItemPrefecture.innerHTML = value.$.title;

    var dropdownMenu = document.createElement('ul');
    dropdownMenu.className = 'dropdown-menu';
    dropdownMenu.setAttribute('aria-labelledby', 'dropdownMenuLink');
    
    _.forEach(value.city, (city, key2) => {
      var li = document.createElement('li');
      var a = document.createElement('a');
      a.className = 'dropdown-item';
      a.href = '#';
      if ('$' in city) {
        a.innerHTML = city.$.title;
        a.id = city.$.id;
      } else {
        a.innerHTML = city.title;
        a.id = city.id;
      }
      a.addEventListener('click', (event) => {
        getWeatherForecast(event.target.id)
        .then(data => {
          parseJson(data);
        });
      })
      li.appendChild(a);
      dropdownMenu.appendChild(li);
    });
    
    dropright.appendChild(dropdownItemPrefecture);
    dropright.appendChild(dropdownMenu);

    document.getElementById('card-header__dropdown--prefectures').appendChild(dropright);
  });
}

async function getWeatherForecast(cityID) {
  const res = await fetch(
    'http://weather.livedoor.com/forecast/webservice/json/v1?city=' + cityID
  );
  const data = await res.json();
  return data;
}

function parseJson(data) {
  document.getElementById('card-header__title').innerHTML = data['title'];
  document.getElementById('card-body__description').innerHTML =
    data.description.text;
  var forecasts_table = document.getElementById(
    'card-body__container--forecasts-table'
  );

  _.forEach(data.forecasts, (value, key) => {
    forecasts_table.rows[1].cells[key].innerHTML = value.telop;
  });

  _.forEach(data.pinpointLocations, (value, key) => {
    var btn = document.createElement('a');
    btn.className = 'btn btn-link';
    btn.type = 'button';
    btn.innerHTML = value.name;
    btn.href = value.link;
    document.getElementById('card-footer__pinpoint-buttons').appendChild(btn);
  });
}

window.onload = function() {
  setCityDropDown();
  
  getWeatherForecast(140010)
  .then(data => {
    parseJson(data);
  });
};
