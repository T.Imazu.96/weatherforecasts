var weatherJson = 0;

async function getWeatherForecast( cityID ) {
    //fetch('http://weather.livedoor.com/forecast/webservice/json/v1?city=' + cityID)
    //.then(function(response) { return response.json() })
    //.then(function(data) {
    //    parseJson(data);
    //})
    //.catch(function() {
    //    console.log('error.')
    //});
    const res = await fetch('http://weather.livedoor.com/forecast/webservice/json/v1?city=' + cityID);
    const data = await res.json();
    return data;
}

async function parseJson(data) {
    weatherJson = data;
    console.log(weatherJson);
    document.getElementById('title').innerHTML = weatherJson['title'];
    document.getElementById('description').innerHTML = weatherJson.description.text;
    var forecasts_table = document.getElementById('forecasts_table');

    for(let k in weatherJson.forecasts) {
        forecasts_table.rows[1].cells[k].innerHTML = weatherJson.forecasts[k].telop;
    }

    for (let k in weatherJson.pinpointLocations) {
        var btn = document.createElement('BUTTON');
        btn.textContent = weatherJson.pinpointLocations[k].name;
        btn.addEventListener('click', jump);
        document.getElementById('sample').appendChild(btn);
    }
}

function jump() {
    var obj = event.target;
    for (let k in weatherJson.pinpointLocations)
        if (weatherJson.pinpointLocations[k].name == obj.innerHTML)
            if (confirm(obj.innerHTML + 'のピンポイント天気予報に移動しますか？')==true)
                location.href = weatherJson.pinpointLocations[k].link;
}

window.onload = function() {
    //getWeatherForecast( 140010 )
	//.then(function(data) {
		//parseJson(data);
	//});
    getWeatherForecast( 140010 )
	.then(data => {
		parseJson(data);
	});
}
