var weatherJson = 0;

function getWeatherForecastsCityJson() {
    var json = getWeatherForecastAreaJson();
    console.log(json);
    return json.rss.channel["ldWeather:source"].pref;
  }

function setCityDropDown() {
    var json = getWeatherForecastsCityJson();
    
    // _.forEach(json, (value, key) => {
    //     var dropdown = document.createElement('li');
    //     dropdown.className = 'dropright drop-hover';
    //     var dropdownItemPrefecture = document.createElement('')
    // });
    

}

async function getWeatherForecast(cityID) {
  const res = await fetch(
    'http://weather.livedoor.com/forecast/webservice/json/v1?city=' + cityID
  );
  const data = await res.json();
  return data;
}

function parseJson(data) {
  weatherJson = data;
  console.log(weatherJson);
  document.getElementById('card-header__title').innerHTML = weatherJson['title'];
  document.getElementById('card-body__description').innerHTML =
    weatherJson.description.text;
  var forecasts_table = document.getElementById(
    'card-body__container--forecasts-table'
  );

  _.forEach(weatherJson.forecasts, (value, key) => {
    forecasts_table.rows[1].cells[key].innerHTML = value.telop;
  });

  _.forEach(weatherJson.pinpointLocations, (value, key) => {
    var btn = document.createElement('a');
    btn.className = 'btn btn-link';
    btn.type = 'button';
    btn.innerHTML = value.name;
    btn.href = value.link;
    document.getElementById('card-footer__pinpoint-buttons').appendChild(btn);
  });
}

window.onload = function() {
  setCityDropDown();
  
  getWeatherForecast(140010).then(data => {
    parseJson(data);
  });
};
