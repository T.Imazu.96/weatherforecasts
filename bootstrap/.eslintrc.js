module.exports = {
    'env': {
        'browser': true,
        'es6': true
    },
    'plugins': [
        'lodash'
    ],
    'extends': [
        'eslint:recommended',
        'plugin:prettier/recommended',
        'plugin:lodash/recommended'
    ],
    'globals': {
        'Atomics': 'readonly',
        'SharedArrayBuffer': 'readonly'
    },
    'parserOptions': {
        'ecmaVersion': 2018,
        'sourceType': 'module'
    },
    'rules': {
        'prettier/prettier': [
            'error',
            {
                'singleQuote': true,
                'trailingComma': 'es5'
	    }
	],
        'no-console': 'warn',
        'indent': [
            'error',
            2
        ],
        'linebreak-style': [
            'error',
            'windows'
        ],
        'quotes': [
            'error',
            'single'
        ],
        'semi': [
            'error',
            'always'
        ]
    }
};
